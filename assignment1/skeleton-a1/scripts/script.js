"use strict";

document.addEventListener("DOMContentLoaded", setup);

function setup() {
    document.querySelector('button').addEventListener("click", createTable);
}


function createTable() {

    /* Store input and table elements inside variables */

    var tblSect = document.getElementById("tableHere");
    var tbl = document.createElement("table");

    var nbRow = document.getElementById("rcount").value;
    var nbCol = document.getElementById("ccount").value;

    var oddColor = document.getElementById("oddcolor").value;
    var evenColor = document.getElementById("evencolor").value;

    /* Remove existing table */
    tblSect.textContent = undefined;


    // generate the table using a for loop
    for (var i = 0; i < nbRow; i++) {

        var row = document.createElement("tr"); // create a row using <tr> element
        for (var j = 0; j < nbCol; j++) {

            // Create table cells using <td> element
            var cell = document.createElement("td");

            /* cell.setAttribute("class", "box"); */

            /* Create text inside the table cells that displays row number and column number */
            if (nbCol < 50) {
                cell.innerHTML = i + ", " + j;
            }


            // Change background color for even and odd cells
            if ((i + j) % 2 == 0) {
                cell.style.backgroundColor = evenColor;
            }
            else {
                cell.style.backgroundColor = oddColor;
            }

            row.appendChild(cell);
        }

        tbl.appendChild(row);

    }

    tblSect.appendChild(tbl);

}